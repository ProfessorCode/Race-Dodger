import pgzrun
import logging
from player import Player
from enemy import Enemies

TITLE = 'Race Dodger'
WIDTH = 600
HEIGHT = 850


def draw():
    screen.clear()
    game.draw()


def update():
    game.update()


class RaceDodger:
    def __init__(self):
        self.player = Player(WIDTH, HEIGHT)
        self.enemies = Enemies(WIDTH, HEIGHT)
        self.game_over = False
        self.score = 0
        self.difficulty_threshold = 1

    def check_collisions(self):
        for enemy in self.enemies.cars:
            if self.player.image.colliderect(enemy):
                self.game_over = True

    def update(self):
        if not self.game_over:
            self.enemies.generate_enemies()
            self.player.move_player()
            self.enemies.move_enemies(self.player.image.bottom)
            self.check_collisions()
            self.update_score()
            self.update_difficulty()

    def draw(self):
        if not self.game_over:
            self.player.image.draw()
        self.enemies.draw_enemies()
        self.draw_score()

    def update_score(self):
        self.score = len(self.enemies.passed_player)

    def draw_score(self):
        pos = WIDTH - 10, 30
        screen.draw.text(str(self.score), midright=pos, fontsize=70)

    def update_difficulty(self):
        current_difficulty = self.score / 10

        if current_difficulty == self.difficulty_threshold:
            logging.info('increase difficulty')
            self.enemies.y_speed += 1
            self.difficulty_threshold += 1


game = RaceDodger()
pgzrun.go()
