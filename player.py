from pgzero.builtins import *


class Player:
    def __init__(self, screen_width, screen_height):
        self.width, self.height = screen_width, screen_height
        self.image = Actor('player')
        self.image.midbottom = self.width/2, self.height - 5
        self.x_speed = 5
        self.y_speed = 10

    def move_player(self):
        if keyboard.right:
            self.image.x += self.x_speed
        if keyboard.left:
            self.image.x -= self.x_speed

        if keyboard.up:
            self.image.y -= self.y_speed
        if keyboard.down:
            self.image.y += self.y_speed

        self.check_boundary()

    def check_boundary(self):
        if self.image.right >= self.width:
            self.image.right = self.width
        elif self.image.left <= 0:
            self.image.left = 0

        if self.image.bottom >= self.height:
            self.image.bottom = self.height
        elif self.image.top <= 0:
            self.image.top = 0
