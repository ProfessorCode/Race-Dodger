# Race Dodger

Race around the street as you dodge incoming cars.

## Copyright and Attribution

### Images
- Cars - [Kenney.nl](https://kenney.nl/)

## License

Licensed under **GPL v3 or later**. Please refer to `LICENSE` for more information.