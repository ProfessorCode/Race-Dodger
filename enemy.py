from pgzero.builtins import *
from random import randint
import logging

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(levelname)s - %(message)s'
)
logging.disable(logging.CRITICAL)


class Enemies:
    def __init__(self, width, height):
        self.width, self.height = width, height
        self.cars = []
        self.car_positions = {}
        self.max_cars = 4
        self.y_speed = 4
        self.passed_player = []

    def move_enemies(self, player_pos_y):
        logging.info(f'enemy y_speed = {self.y_speed}')
        for car in self.cars:
            car.y += self.y_speed
            self.check_player(car, player_pos_y)
            self.check_boundary(car)

    def generate_enemies(self):
        logging.debug(f'cars = {len(self.cars)}')

        while len(self.cars) < self.max_cars:
            car = Actor('enemy')
            car.angle = 180
            x_pos = randint(20, self.width - 20)
            overlaps = self.check_cars_overlap(x_pos, car.width)

            if overlaps:
                continue

            self.car_positions[car] = x_pos
            y_pos = randint(-1000, 0)
            car.midtop = x_pos, y_pos
            self.cars.append(car)

    def draw_enemies(self):
        for car in self.cars:
            logging.debug(f'Draw = {car.x}, {car.y}')
            car.draw()

    def check_cars_overlap(self, x_pos, car_width):
        overlaps = False

        for pos in self.car_positions.values():
            overlap_right = x_pos >= pos and x_pos <= pos + car_width*1.5
            overlap_left = x_pos <= pos and x_pos >= pos - car_width*1.5

            if overlap_left or overlap_right:
                overlaps = True
                break

        return overlaps

    def check_boundary(self, car):
        if car.top > self.height:
            self.cars.remove(car)
            del self.car_positions[car]

    def check_player(self, car, player_pos_y):
        if car.top > player_pos_y and car not in self.passed_player:
            self.passed_player.append(car)
